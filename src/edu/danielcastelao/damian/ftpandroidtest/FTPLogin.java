package edu.danielcastelao.damian.ftpandroidtest;

import network.ConnectionManager;
import parser.ftp.FTPParser;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

@SuppressWarnings("unused")
public class FTPLogin extends Activity implements OnClickListener{
	private EditText editUsername;
	private EditText editPassword;
	private EditText editHostname;
	private EditText editPort;
	private CheckBox checkSave;
	private CheckBox checkAnon;
	private Button buttonConnect;
	
	@Override // Activity
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.initUI();
		this.addListeners();
	}

	private void initUI() {
		setContentView(R.layout.cool_layout);
		editUsername = (EditText) findViewById(R.id.ly_login_editUsername);
		editPassword = (EditText) findViewById(R.id.ly_login_editPass);
		editHostname = (EditText) findViewById(R.id.ly_login_editHost);
		editPort = (EditText) findViewById(R.id.ly_login_editPort);
		
		checkSave = (CheckBox) findViewById(R.id.ly_login_checkSave);
		checkAnon = (CheckBox) findViewById(R.id.ly_login_checkAnon);
		
		// default fill
		editHostname.setText("paxinas.danielcastelao.org");
		editPort.setText("21");
		editUsername.setText("ycarballoporto");
		editPassword.setText(new String(new char[]{'3', '6', '1', '5', '3', '1', '8', '1', 'X'}));
		checkSave.setSelected(true);
		
		buttonConnect = (Button) findViewById(R.id.ly_login_buttonConnect);
	}

	private void addListeners() {
		checkSave.setOnClickListener(this);
		checkAnon.setOnClickListener(this);
		buttonConnect.setOnClickListener(this);
	}

	@Override // Activity
	public void onClick(View view) {
		if(view == buttonConnect) {
			try {
				String host		= editHostname.getText().toString();
				int port		= Integer.parseInt(editPort.getText().toString());
				String username = editUsername.getText().toString();
				char[] password = editPassword.getText().toString().toCharArray();
				
				Intent ftp_intent = new Intent(FTPLogin.this, FTP_View.class);
				ftp_intent.putExtra("host", host);
				ftp_intent.putExtra("port", port);
				ftp_intent.putExtra("username", username);
				ftp_intent.putExtra("password", password);
				startActivity(ftp_intent);
				Toast.makeText(this, "Loading..., (This may Take a while)", Toast.LENGTH_SHORT).show();
				
			} catch (NumberFormatException e) {
				Toast.makeText(this, "Wrong Port", Toast.LENGTH_LONG).show();
			}
        	
		} else if(view == checkAnon) {
			if(checkAnon.isChecked()) {
				editUsername.setText("Anonymous");
				editPassword.setText("anonymous");
			
			} else {
				editUsername.setText("");
				editPassword.setText("");
			}
		
		} else if(view == checkAnon) {
			
		}
	}
}

