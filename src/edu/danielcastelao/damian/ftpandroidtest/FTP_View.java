package edu.danielcastelao.damian.ftpandroidtest;

import java.io.File;
import java.util.LinkedList;

import network.ConnectionCore;
import parser.Settings;
import parser.entities.CFile;
import parser.entities.CFolder;
import parser.entities.Item;
import parser.ftp.FTPParser;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import edu.danielcastelao.damian.ftpandroidtest.core.AsyncHandler;

public class FTP_View extends Activity implements OnItemClickListener {
	private ListView fileList = null;
	private ProgressBar progressBar = null;
	private CFolder root = null;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ftp__view);
        
        this.fileList = (ListView) this.findViewById(R.id.fileList);
        this.progressBar = (ProgressBar) this.findViewById(R.id.progressBar);
        this.progressBar.setMax(100);
        this.progressBar.setIndeterminate(true);
        
        if (fileList != null) {
        	Settings.PREFS.putInt("connection_type", ConnectionCore.PASSIVE);
        	this.fileList.setOnItemClickListener(this);
        	this.updateList(new CFolder("").getChilds());
        	
        	Intent intent = getIntent();
        	String host		= intent.getStringExtra("host");
			int port		= intent.getIntExtra("port", 21);
			String username = intent.getStringExtra("username");
			char[] password = intent.getCharArrayExtra("password");
			
			IntentFilter filter = new IntentFilter("edu.danielcastelao.damian.ftpandroidtest.intent.updater");
	    	filter.addCategory(Intent.CATEGORY_DEFAULT);
	    	GUIUpdater receiver = new GUIUpdater();
	    	registerReceiver(receiver, filter);
			
			Intent service_intent = new Intent(this, AsyncHandler.class);
			service_intent.putExtra("host", host);
			service_intent.putExtra("port", port);
			service_intent.putExtra("username", username);
			service_intent.putExtra("password", password);
			
			startService(service_intent);
        }
    }
    
    public void updateList (LinkedList<Item> items) {
    	String[] files = new String[items.size()];
    	for (int i=0;i<items.size();i++) {
    		files[i] = items.get(i).getName();
    	}
    	
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, files);
        runOnUiThread(new Runnable() {
      	     public void run() {
      	    	 fileList.setAdapter(adapter);
      	     }
        });
        
        progressBar.setIndeterminate(false);
	}


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.ftp__view, menu);
        return true;
    }

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		String name = (String) parent.getItemAtPosition(position);
		for (Item item: root.getChilds()) {
			if (item.getName().equals(name)) {
				progressBar.setIndeterminate(true);
				if (item instanceof CFolder) {
					FTPParser.list(item.getConnection(), item.getName());
					
				} else if (item instanceof CFile) {
					CFile file = (CFile) item;
					File localFile = file.getLocalFile();
					
					if (localFile == null) {
						File downloads = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
					    if (downloads != null) {
					    	if (!downloads.isDirectory()) file.setLocalFile(downloads);
					    	else {
					    		String bar = (downloads.getAbsolutePath().endsWith("/")?"":"/");
					    		file.setLocalFile(new File(downloads.getAbsolutePath()+bar+file.getName()));
					    	}
					    }
					}
					
					FTPParser.download(item.getConnection(), file);
				} break;
			}
		}
	}
	
	public void setRoot(CFolder folder) {
		root = folder;
		updateList(root.getChilds());
	}
	
	public void updateProgress(final int value) {
		runOnUiThread(new Runnable() {
	        @Override
	        public void run() {
	        	progressBar.setProgress(value);
	    		if (value == 100) {
	    			Toast.makeText(getApplicationContext(), "Your download has Finished!!", Toast.LENGTH_LONG).show();
	        	} else if (value == 0) {
	    			progressBar.setIndeterminate(false);
	        	}
	        }
		});
	}
	
	private class GUIUpdater extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getBooleanExtra("update_dir", false)) {
				setRoot((CFolder) intent.getSerializableExtra("folder"));
				
			} else {
				updateProgress(intent.getIntExtra("value", 0));
				
			}
		}

	}
}
