package edu.danielcastelao.damian.ftpandroidtest.core;

import network.ConnectionDelegate;
import network.ConnectionManager;
import parser.ParserListener;
import parser.entities.CFolder;
import parser.ftp.FTPParser;
import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

public class AsyncHandler extends IntentService implements ConnectionDelegate, ParserListener {
	private String host 			= "127.0.0.1";
	private int port				= 21;
	private String username			= "anonymous";
	private char[] password			= new char[0];

	public AsyncHandler() {
		super("AsyncHandler");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		host		= intent.getStringExtra("host");
		port		= intent.getIntExtra("port", 21);
		username	= intent.getStringExtra("username");
		password	= intent.getCharArrayExtra("password");
		
		new FTPParser();
    	ConnectionManager.instance().addConnectionListener(this);
    	ConnectionManager.instance().addFolderParserListener(this);
    	ConnectionManager.instance().createConnectionAndStart(host, port, username, password, host);
	}

	@Override
	public boolean connectionIsListeningTo(String name) {
		return true;
	}


	@Override
	public void connectionLineQueued(String name, String line, int responseType) {
		Log.i("Yebud::connectionLineQueued", "Line Added to Queue for ("+name+"), Line, ("+responseType+"): "+line);
	}


	@Override
	public void connectionLineReceived(String name, String line) {
		Log.i("Yebud::connectionLineReceived", "Line Received ("+name+"), Size: "+line);
	}


	@Override
	public void connectionRawDataReceived(String name, int size, byte[] buffer, int left) {
		Log.i("Yebud::connectionRawDataReceived", "Raw Data Received ("+name+"), Size: "+size);
	}


	@Override
	public void connectionStatusChanged(String name, int status) {
		Log.i("Yebud::connectionStatusChanged", "Status Changed for ("+name+"): "+status);
	}

	@Override
	public void folderParsed(String name, CFolder root) {
		Intent broadcastIntent = new Intent();
		broadcastIntent.setAction("edu.danielcastelao.damian.ftpandroidtest.intent.updater");
		broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
		broadcastIntent.putExtra("update_dir", true);
		broadcastIntent.putExtra("folder", root);
		sendBroadcast(broadcastIntent);
	}

	@Override
	public void showMessage(String name, String message, String title) {}

	@Override
	public void transferProgress(String name, int value) {
		Intent broadcastIntent = new Intent();
		broadcastIntent.setAction("edu.danielcastelao.damian.ftpandroidtest.intent.updater");
		broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
		broadcastIntent.putExtra("update_dir", false);
		broadcastIntent.putExtra("value", value);
		sendBroadcast(broadcastIntent);
	}
}
